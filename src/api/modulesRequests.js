import { axiosInstance } from "../library/services/net/axiosGuest";

export const modulesRequests = {
  async checkBulling(text) {
    try {
      const bullingResponse = await axiosInstance.post("/bulling/bulling/", {
        text
      });
      return bullingResponse.data;
    } catch (e) {
      console.log(e);
    }
  },
  async checkEmail(email) {
    try {
      const bullingResponse = await axiosInstance.post(
        "/email_validator/email_validator/",
        { email }
      );
      const { status } = bullingResponse;
      if (status === 200)
        return {
          success: true,
          detailed: bullingResponse.data
        };
    } catch (e) {
      return {
        success: false,
        detailed: e.response?.data
      };
    }
  },
  async sendFile(url, file) {
    const response = await axiosInstance.post(url, file, {
      headers: {
        'Content-Type': 'file',
        "Accept": "*/*"
      }
    });

    return response?.data || response;
  },
  async decodeValue(value) {
    try {
      const response = await axiosInstance.post(
          "http://78.24.216.177:8000/decryptor/decryptor/",
          { to_decrypt: value }
      );
      return response.data;
    } catch (e) {
      return null;
    }
  },
  async scanUrl(url) {
    try {
      const scanningResponse = await axiosInstance.post(
        "http://78.24.216.177:8000/vulnerability/find-vulnerability/",
        { url }
      );
      return scanningResponse.data;
    } catch (e) {
      return null;
    }
  }
};

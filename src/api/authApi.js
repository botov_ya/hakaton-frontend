import { axiosInstance } from "../library/services/net/axiosGuest";

export const authApi = {
  async refreshToken() {
    const response = await axiosInstance.post(
      "/api/token/refresh/",
      {
        refresh: localStorage.getItem("token_refresh"),
        username: localStorage.getItem("user"),
      },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );
    if (response.data.message) {
      return false;
    } else {
      localStorage.setItem("token_access", response.data.access);
      localStorage.setItem("token_refresh", response.data.refresh);
      return true;
    }
  },
  async login(username, password) {
    try {
      const response = await axiosInstance.post(
        "/api/token/",
        {
          username,
          password,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }
      );
      localStorage.setItem("access_token", response.data.access);
      localStorage.setItem("refresh_token", response.data.refresh);
      localStorage.setItem("user", username);
      return true;
    } catch (e) {
      return false;
    }
  },
  async registerUser({ username, password, repeat_password }) {
    const response = await axiosInstance.post(
      "/bank/registration/",
      {
        username,
        password,
        repeat_password,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );
    return response;
  },
};

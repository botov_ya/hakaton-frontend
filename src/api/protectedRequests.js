import securedAxiosInstance from "../library/services/net/axiosSecured";

export const protectedRequests = {
  async createCard(text) {
    try {
      const cardsResponse = await securedAxiosInstance.put("/bank/cards/ ", {
        text,
      });
      return cardsResponse.data;
    } catch (e) {
      return [];
    }
  },
  async getCards() {
    try {
      const cardsResponse = await securedAxiosInstance.get("/bank/cards/");
      return cardsResponse.data;
    } catch (e) {
      return [];
    }
  },
  async getTransactions(cardId) {
    try {
      const transactionsResponse = await securedAxiosInstance.get(
        `/bank/transactions/?card=${cardId}`
      );
      return transactionsResponse.data;
    } catch (e) {
      return [];
    }
  },
  async createTransaction(text) {
    try {
      const bullingResponse = await securedAxiosInstance.put(
        "/bank/transactions/",
        {
          text,
        }
      );
      return bullingResponse.data;
    } catch (e) {
      console.log(e);
    }
  },
};

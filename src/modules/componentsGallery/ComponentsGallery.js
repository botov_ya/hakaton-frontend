import React, { useState } from "react";
import Collapse from "@material-ui/core/Collapse";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { LineChartBuilder } from "../../library/UI-elements/charts/LineChartBuilder";
import { AreaChartBuilder } from "../../library/UI-elements/charts/AreaChartBuilder";
import { BarChartBuilder } from "../../library/UI-elements/charts/BarChartBuilder";
import { RadialChartBuilder } from "../../library/UI-elements/charts/RadialChartBuilder";
import CustomAlert from "../../library/UI-elements/CustomAlert/CustomAlert";
import { alertTypesEnum } from "../../library/enums/alertTypesEnum";
import PreviewCard from "../../library/UI-elements/PreviewCard/PreviewCard";
import { PieChartBuilder } from "../../library/UI-elements/charts/PieChartBuilder";
import { RadarAnimation } from "../../library/UI-elements/RadarAnimation/RadarAnimation";

const REPRESENTATION_COMPONENTS = [
  <LineChartBuilder useMockData={true} />,
  <AreaChartBuilder useMockData={true} />,
  <BarChartBuilder useMockData={true} />,
  <RadialChartBuilder useMockData={true} />,
  <RadialChartBuilder useMockData={true} showAsRainbow={false} />,
  <CustomAlert
    type={alertTypesEnum.success}
    title={"Test"}
    message={"Test message"}
    size={{ width: "450px" }}
    isOpen={true}
    onClose={() => {}}
  />,
  <PreviewCard
    title={"Decoder"}
    size={{ width: "300px", minHeight: "500px", height: "100%" }}
  />,
  <PieChartBuilder useMockData={true} />,
  <RadarAnimation />,
];

const prepareOpenIndexesList = () => {
  const result = [];
  for (let it in REPRESENTATION_COMPONENTS) {
    result.push({ index: it, opened: false });
  }
  return result;
};

const ComponentsGallery = (props) => {
  const [closeStatuses, setCloseStatuses] = useState(prepareOpenIndexesList());

  const toggleOpenCollapse = (index) => {
    setCloseStatuses((prevState) => {
      const copy = [...prevState];
      copy[index] = {
        ...copy[index],
        opened: !prevState[index].opened,
      };
      return copy;
    });
  };

  const componentCards = REPRESENTATION_COMPONENTS.map((component, index) => {
    return (
      <>
        <ListItem button onClick={() => toggleOpenCollapse(index)}>
          <ListItemText primary={component?.type?.name} />
          {closeStatuses[index]?.opened ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse
          in={closeStatuses[index]?.opened ?? false}
          timeout="auto"
          unmountOnExit
        >
          {component}
        </Collapse>
      </>
    );
  });

  return <div>{componentCards}</div>;
};

ComponentsGallery.propTypes = {};

export default ComponentsGallery;

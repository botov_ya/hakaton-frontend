import React from "react";
import {useDefaultModuleEntryPoint} from "../../library/hooks/useDefaultModuleEntryPoint";
import ComponentsGallery from "./ComponentsGallery";


export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <ComponentsGallery />,
    withAppBar: true
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

import React from "react";
import TextField from "@material-ui/core/TextField"
import InputsArea from "../../../../library/UI-elements/InputsArea/InputsArea";

const InputsGroup = ({ handleSendRequest, handleChangeTextField, url }) => {
    const inputsRender = (
        <TextField
            style={{width:'100%'}}
            placeholder="https://attack.me"
            variant="outlined"
            value={url}
            onChange={handleChangeTextField}
        />
    );
    return (
        <InputsArea inputsRender={inputsRender} handleClick={handleSendRequest} btnText={"Проверить"} />
    );
};

export default InputsGroup;
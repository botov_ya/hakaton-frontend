import React, { useContext, useState } from "react";
import { modulesRequests } from "../../../api/modulesRequests";
import Box from "@material-ui/core/Box";
import { RadarAnimation } from "../../../library/UI-elements/RadarAnimation/RadarAnimation";
import Backdrop from "@material-ui/core/Backdrop";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import InputGroup from "./Groups/InputGroup";
import { GlobalContext } from "../../../root/App";
import { ReportCard } from "./ReportCard/ReportCard";
import { alertTypesEnum } from "../../../library/enums/alertTypesEnum";

const UrlScanner = props => {
  const { globalStateHandlers, globalState } = useContext(GlobalContext);
  const {
    handleSetGlobalFetchingStatus,
    handleSetGlobalAlert
  } = globalStateHandlers;
  const [url, setUrl] = useState("");
  const [attackReport, setAttackReport] = useState(null);

  const handleChangeTextField = event => {
    const { value } = event.currentTarget;
    setUrl(value);
  };

  const handleSendRequest = () => {
    handleSetGlobalFetchingStatus(true);
    const errorMessage = {
      status: "active",
      type: alertTypesEnum.error,
      title: "Произошла ошибка",
      message: "Проверьте корректность введеного url"
    };
    modulesRequests
      .scanUrl(url)
      .then(result => {
        if (result) {
          setAttackReport(result);
        } else {
          handleSetGlobalAlert(errorMessage);
          setAttackReport(null);
        }
      })
      .catch(e => {
        console.error(e);
        handleSetGlobalAlert(errorMessage);
        setAttackReport(null);
      })
      .finally(() => {
        handleSetGlobalFetchingStatus(false);
        setUrl("");
      });
  };

  const buildAttackReport = attackReport => {
    const sqlAttacks = [];
    const idorAttacks = [];
    const xssAttacks = [];
    if (attackReport) {
      for (let report of attackReport) {
        switch (report.type) {
          case "sql_injection": {
            report.data.forEach(value => sqlAttacks.push(<p>{value}</p>));
            break;
          }
          case "xss": {
            report.data.forEach(value => xssAttacks.push(<p>{value}</p>));
            break;
          }
          case "idor": {
            report.data.forEach(value => idorAttacks.push(<p>{value}</p>));
            break;
          }
        }
      }
    }
    return { sqlAttacks, idorAttacks, xssAttacks };
  };

  const { sqlAttacks, idorAttacks, xssAttacks } = buildAttackReport(
    attackReport
  );

  return (
    <div>
      <Backdrop open={globalState.isFetching}>
        <RadarAnimation />
      </Backdrop>
      <InputGroup
        handleSendRequest={handleSendRequest}
        handleChangeTextField={handleChangeTextField}
        url={url}
      />
      <Box mt={5}>
        {attackReport && (
          <>
            <ReportCard
              sqlAttacks={sqlAttacks}
              idorAttacks={idorAttacks}
              xssAttacks={xssAttacks}
            />
          </>
        )}
      </Box>
    </div>
  );
};

export default UrlScanner;

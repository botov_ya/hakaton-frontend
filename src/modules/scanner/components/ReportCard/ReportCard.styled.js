import styled from "styled-components";
import Box from "@material-ui/core/Box";

const LocalCardItem = styled.div`
  display: flex;
  align-items: center;
  margin: 0 -10px;
  & > * {
    margin: 0 10px;
  }
`;

const ExtendedMUIBox = styled(Box)`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: center;
  margin: 0 -15px;
  & ${LocalCardItem} {
    margin: 0 15px;
  }
`;

export const Styled = { ExtendedMUIBox, LocalCardItem };

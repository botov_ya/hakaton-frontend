import React from "react";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import { Box } from "@material-ui/core";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import { Styled } from "./ReportCard.styled";

const { ExtendedMUIBox, LocalCardItem } = Styled;

export const ReportCard = ({ sqlAttacks, idorAttacks, xssAttacks }) => {
  return (
    <Card>
      <CardContent>
        <Typography align={"center"} component="h6" variant="h6">
          Отчет по уязвимостям
        </Typography>
          <br />
        <ExtendedMUIBox p={1}>
          <LocalCardItem>
            <section>
              {sqlAttacks.length ? (
                <HighlightOffIcon color={"error"} />
              ) : (
                <DoneOutlineIcon color={"primary"} />
              )}
            </section>
            <section>
              <Typography variant={"h6"} component={"h6"}>
                SQL уязвимости
              </Typography>
              <p>{sqlAttacks.length ? sqlAttacks : "Не обнаружено"}</p>
            </section>
          </LocalCardItem>
          <LocalCardItem>
            <section>
              {idorAttacks.length ? (
                <HighlightOffIcon color={"error"} />
              ) : (
                <DoneOutlineIcon color={"primary"} />
              )}
            </section>
            <section>
              <Typography variant={"h6"} component={"h6"}>
                idor уязвимости
              </Typography>
              <p>{idorAttacks.length ? idorAttacks : "Не обнаружено"}</p>
            </section>
          </LocalCardItem>
          <LocalCardItem>
            <section>
              {xssAttacks.length ? (
                <HighlightOffIcon color={"error"} />
              ) : (
                <DoneOutlineIcon color={"primary"} />
              )}
            </section>
            <section>
              <Typography variant={"h6"} component={"h6"}>
                XSS уязвимости
              </Typography>
              {xssAttacks.length ? xssAttacks : "Не обнаружено"}
            </section>
          </LocalCardItem>
        </ExtendedMUIBox>
      </CardContent>
    </Card>
  );
};

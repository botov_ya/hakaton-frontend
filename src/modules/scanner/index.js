import React from "react";
import UrlScanner from "./components/UrlScanner";
import { useDefaultModuleEntryPoint } from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <UrlScanner />,
    withAppBar: true,
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

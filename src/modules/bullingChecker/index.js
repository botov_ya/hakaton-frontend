import React from "react";
import BullingChecker from "./components/BullingChecker";
import {useDefaultModuleEntryPoint} from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <BullingChecker />,
    withAppBar: true
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

import React from 'react';
import Box from "@material-ui/core/Box";
import ContentPresenter from "../../../../library/UI-elements/ContentPresenter/ContentPresenter";
import {PieChartBuilder} from "../../../../library/UI-elements/charts/PieChartBuilder";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";
import Divider from "antd/lib/divider";

const StyledWordsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const BullingContentGroup = ({bullingResult, preparedDiagramData, bullingPercent, generatedWordReport,  children}) => {
    return (
        <Box mt={5} style={{height: '100%'}}>
            {bullingResult && (
                <>
                    <ContentPresenter>
                        <div>
                            <Typography style={{width: '100%', marginBottom: '10px'}} align={"center"} component={'h6'} variant={'h6'}>Процент буллинга: </Typography>
                            <Typography align={"center"}>{bullingPercent + '%'}</Typography>
                        </div>
                        <StyledWordsContainer>
                            <Typography style={{width: '100%', marginBottom: '10px'}} align={"center"} variant={'h6'} component={'h6'}>Эмоциональный окрас слов: </Typography>
                            <Typography style={{display: 'flex', flexWrap: 'wrap'}} align={"center"}>{generatedWordReport}</Typography>
                        </StyledWordsContainer>
                        <PieChartBuilder
                            data={preparedDiagramData}
                            showLegend={false}
                            showTooltip={false}
                        />
                    </ContentPresenter>
                </>
            )}
        </Box>
    );
};

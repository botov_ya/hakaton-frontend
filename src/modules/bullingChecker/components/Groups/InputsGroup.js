import React from "react";
import TextField from "@material-ui/core/TextField";
import styled from "styled-components";
import InputsArea from "../../../../library/UI-elements/InputsArea/InputsArea";

const StyledTextField = styled(TextField)`
  width: 100%;
`;

export const InputsGroup = ({
  handleSendRequest,
  handleChangeTextField,
  textFieldVal
}) => {
  const inputsRender = (
    <>
      <StyledTextField
        variant="outlined"
        placeholder={"Введите текст для проверки буллинга"}
        value={textFieldVal}
        onChange={handleChangeTextField}
        multiline
      />
    </>
  );
  return <InputsArea inputsRender={inputsRender} handleClick={handleSendRequest} />;
};

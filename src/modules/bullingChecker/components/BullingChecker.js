import React, { useState } from "react";
import { modulesRequests } from "../../../api/modulesRequests";
import { getColorInRangeRedGreen } from "../../../library/services/getColorInRangeRedGreen";
import styled from "styled-components";
import * as d3Color from "d3-color";
import { BullingContentGroup } from "./Groups/ContentGroup";
import { InputsGroup } from "./Groups/InputsGroup";

const StyledWord = styled.p`
  background-color: ${props => props.color};
  color: ${props => props.textColor};
  margin: 0;
  margin-right: 5px;
`;

const generateBullingWordsReport = (text, report) => {
  const words = text.split(" ");
  return words.map(word => {
    const { bad_words } = report;
    const preparedWord = word.toLowerCase().trim();
    const bullingCoeff = bad_words[preparedWord] ?? null;
    const color = bullingCoeff
      ? getColorInRangeRedGreen(bullingCoeff, 1)
      : "white";
    const hsl = d3Color.hsl(d3Color.color(color));
    const lightness = hsl.l;
    const textColor = lightness > 0.5 ? "black" : "whitesmoke";
    return (
      <StyledWord color={color} textColor={textColor}>
        {word + " "}
      </StyledWord>
    );
  });
};

const getBullingChance = bullingReport => {
  return (Number.parseFloat(bullingReport.probability_bad) * 100).toFixed(2);
};

const BullingChecker = props => {
  const [text, setText] = useState("");
  const [isFetching, setIsFetching] = useState(false);
  const [bullingResult, setBullingResult] = useState(null);
  const handleChangeTextField = event => {
    const { value } = event.currentTarget;
    setText(value);
    setBullingResult(null);
  };

  const handleSendRequest = () => {
    setIsFetching(true);
    modulesRequests.checkBulling(text).then(result => {
      setBullingResult(result);
      setIsFetching(false);
    });
  };

  const GeneratedWordReport = bullingResult
    ? generateBullingWordsReport(text, bullingResult)
    : null;
  const bullingPercent = bullingResult
    ? Number.parseFloat(getBullingChance(bullingResult))
    : null;
  const preparedDiagramData = [
    {
      name: "Bulling",
      value: bullingPercent,
      fill: "#FF533C"
    },
    {
      name: "Not bulling",
      value: 100 - bullingPercent,
      fill: "#409145"
    }
  ];

  return (
    <div style={{ height: "fit-content" }}>
      <InputsGroup
        handleSendRequest={handleSendRequest}
        handleChangeTextField={handleChangeTextField}
        textFieldVal={text}
      />
      <BullingContentGroup
        bullingResult={bullingResult}
        generatedWordReport={GeneratedWordReport}
        preparedDiagramData={preparedDiagramData}
        bullingPercent={bullingPercent}
      />
    </div>
  );
};

export default BullingChecker;

import styled from "styled-components";
import PreviewCard from "../../../library/UI-elements/PreviewCard/PreviewCard";
import { Link } from "react-router-dom";

const ExtendedLink = styled(Link)``;

const ExtendedPreviewCard = styled(PreviewCard)`
  min-width: 300px;
  width: 100%;
  height: 100%;
  transition: all 0.3s;
  &:hover {
    & svg {
      fill: #3f51b5;
    }
    box-shadow: 0px 0px 10px #3f51b5;
  }
`;

const LocalMainWrapper = styled.section`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const LocalInner = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  padding: 1.5rem 0;
  margin: -10px -10px;
  & ${ExtendedLink} {
    margin: 10px 10px;
  }
`;

export const Styled = {
  LocalMainWrapper,
  LocalInner,
  ExtendedPreviewCard,
  ExtendedLink
};

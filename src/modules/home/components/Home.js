import React, { useContext } from "react";
import { GlobalContext } from "../../../root/context/GlobalContext";
import SpellcheckIcon from "@material-ui/icons/Spellcheck";
import FaceIcon from "@material-ui/icons/Face";
import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";
import DeveloperModeIcon from "@material-ui/icons/DeveloperMode";
import DeveloperBoardIcon from "@material-ui/icons/DeveloperBoard";
import PermScanWifiIcon from "@material-ui/icons/PermScanWifi";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import { Styled } from "./Home.styled";

const {
  LocalMainWrapper,
  LocalInner,
  ExtendedLink,
  ExtendedPreviewCard,
} = Styled;

const iconRenderStyles = { fontSize: "50px" };

const AppModules = [
  {
    iconRender: <SpellcheckIcon style={iconRenderStyles} />,
    title: "Декодер",
    urlTo: "/decoder",
  },
  {
    iconRender: <FaceIcon style={iconRenderStyles} />,
    title: "Буллинг",
    urlTo: "/bulling_checker"
  },
  {
    iconRender: <AlternateEmailIcon style={iconRenderStyles} />,
    title: "Валидатор e-mail",
    urlTo: "/email_checker",
  },
  {
    iconRender: <DeveloperModeIcon style={iconRenderStyles} />,
    title: "Сервис для XSLT преобразований",
    urlTo: "/xslt_adapter",
  },
  {
    iconRender: <PermScanWifiIcon style={iconRenderStyles} />,
    title: "Сканер уязвимостей сайта",
    urlTo: "/site_scanner",
  },
  {
    iconRender: <AccountBalanceIcon style={iconRenderStyles} />,
    title: "Банковское API",
    urlTo: "/bank",
  },
];

const Home = (props) => {
  const global = useContext(GlobalContext);
  const moduleCardsRender = AppModules.map((module) => {
    const { iconRender, title, urlTo } = module;
    return (
      <ExtendedLink to={urlTo}>
        <ExtendedPreviewCard title={title} iconRender={iconRender} />
      </ExtendedLink>
    );
  });

  return (
    <LocalMainWrapper>
      <LocalInner>{moduleCardsRender}</LocalInner>
    </LocalMainWrapper>
  );
};

export default Home;

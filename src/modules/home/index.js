import React from "react";
import Home from "./components/Home";
import { useDefaultModuleEntryPoint } from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <Home />,
    withAppBar: false
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

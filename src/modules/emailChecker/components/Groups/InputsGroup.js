import React from "react";
import TextField from "@material-ui/core/TextField";
import InputsArea from "../../../../library/UI-elements/InputsArea/InputsArea";
import styled from "styled-components";

const StyledTextField = styled(TextField)`
  width: 100%;
`;

const InputsGroup = ({
  emailValue,
  isValidEmail,
  handleChangeTextField,
  handleSendRequest
}) => {
  const inputsRender = (
    <StyledTextField
      error={emailValue && !isValidEmail}
      placeholder="Введите электронную почту"
      variant="outlined"
      value={emailValue}
      onChange={handleChangeTextField}
    />
  );
  return <InputsArea inputsRender={inputsRender} handleClick={handleSendRequest} />;
};

export default InputsGroup;

import React, { useContext, useState } from "react";
import { modulesRequests } from "../../../api/modulesRequests";
import Box from "@material-ui/core/Box";
import styled from "styled-components";
import ReactCardFlip from "react-card-flip";
import DoneOutlineIcon from "@material-ui/icons/DoneOutline";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import InputsGroup from "./Groups/InputsGroup";
import { makeStyles } from "@material-ui/core/styles";
import {Card, Typography} from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import { GlobalContext } from "../../../root/App";

function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const StyledCard = styled(Card)`
  //display: flex;
  //width: 150px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-width: 300px;
  min-height: 300px;
  border: 1px dot-dash ${props => (props.success ? "#388c1c" : "#cc1c16")};
  padding: 15px;
  //flex-direction: column;
  border-radius: 10px;
  // background-color: ${props => (props.success ? "#388c1c" : "#cc1c16")};
  & > div {
    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
  }
`;

const useStyles = makeStyles(({ spacing }) => ({
  content: {
    paddingTop: 0,
    textAlign: "left",
    overflowX: "auto",
    "& table": {
      marginBottom: 0
    }
  }
}));

const EmailChecker = props => {
  const classes = useStyles();
  const { globalStateHandlers, globalState } = useContext(GlobalContext);
  const { handleSetGlobalFetchingStatus } = globalStateHandlers;
  const [emailValue, setEmailValue] = useState("");
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [emailReport, setEmailReport] = useState(null);
  const [isReportFlipped, setIsReportFlipped] = useState(false);
  const handleChangeTextField = event => {
    const { value } = event.currentTarget;
    setEmailValue(value);
    setIsValidEmail(validateEmail(value));
  };

  const handleSendRequest = () => {
    handleSetGlobalFetchingStatus(true);
    modulesRequests.checkEmail(emailValue).then(result => {
      setEmailReport(result);
      handleSetGlobalFetchingStatus(false);
    });
  };

  return (
    <div>
      <InputsGroup
        emailValue={emailValue}
        isValidEmail={isValidEmail}
        handleChangeTextField={handleChangeTextField}
        handleSendRequest={handleSendRequest}
      />
      <Box mt={5}>
        {emailReport && (
          <ReactCardFlip
            isFlipped={isReportFlipped}
            flipDirection="horizontal"
            containerStyle={{
              margin: "0 auto",
              width: "fit-content"
            }}
          >
            <StyledCard
              onMouseEnter={() => setIsReportFlipped(emailReport.success)}
              success={emailReport.success}
            >
              <div
                style={{
                  margin: "0 auto"
                }}
              >
                {emailReport.success ? (
                  <>
                    <Typography variant={'h5'} component={'h5'}>Валидный e-mail</Typography>
                    <Typography variant={'small'} component={'small'}>hover for more information</Typography>
                    <DoneOutlineIcon
                      style={{ fontSize: "120px", color: "#388c1c" }}
                    />
                  </>
                ) : (
                  <>
                    <Typography variant={'h5'} component={'h5'}>Инвалидный e-mail</Typography>
                    <HighlightOffIcon
                      style={{ fontSize: "150px", color: "#cc1c16" }}
                    />
                  </>
                )}
              </div>
            </StyledCard>
            <StyledCard
              onMouseLeave={() => setIsReportFlipped(!emailReport.success)}
              success={emailReport.success}
            >
              <CardContent className={classes.content}>
                <p>
                  Ответ SMPT сервера:{" "}
                  {emailReport?.detailed?.smtp_response?.[0]}{" "}
                  {emailReport?.detailed?.smtp_response?.[1]}
                </p>
                <p>Домен: {emailReport?.detailed?.domain}</p>
                <p>MX: {emailReport?.detailed?.mail_exchanger}</p>
                {emailReport?.success
                  ? emailReport?.detailed?.result
                  : emailReport?.detailed?.detail && <p> Отчет проверки: </p>}
              </CardContent>
            </StyledCard>
          </ReactCardFlip>
        )}
      </Box>
    </div>
  );
};

// {emailReport?.success
// ? emailReport?.detailed?.result
// : emailReport?.detailed?.detail}
export default EmailChecker;

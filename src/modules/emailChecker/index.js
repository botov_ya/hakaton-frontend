import React from "react";
import EmailChecker from "./components/EmailChecker";
import {useDefaultModuleEntryPoint} from "../../library/hooks/useDefaultModuleEntryPoint";


export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <EmailChecker />,
    withAppBar: true
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

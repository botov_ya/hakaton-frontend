import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import CreditCard from "../CreditCard/CreditCard";

const PurchaseDialog = ({ isOpen, handleCloseDialog, handleSubmitPurchase }) => {
  return (
    <Dialog
      fullWidth={true}
      maxWidth={"md"}
      open={isOpen}
      onClose={handleCloseDialog}
      style={{textAlign: 'center'}}
    >
      <DialogTitle>Оплатите покупку</DialogTitle>
      <CreditCard handleSubmitPurchase={handleSubmitPurchase}/>
    </Dialog>
  );
};

export default PurchaseDialog;

import React from "react";
import InputsArea from "../../../../../library/UI-elements/InputsArea/InputsArea";
import { Button as AntdButton } from "antd";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import {Styled} from "./InputsGroup.styled";

const {ExtendedAntdUploader, LocalAntdButtonInner} = Styled

const InputsGroup = ({ handleSendRequest, handleBeforeUploaderUpdate, handleUploaderRemoveItem }) => {
  const inputsRender = (
    <ExtendedAntdUploader onRemove={handleUploaderRemoveItem} beforeUpload={(file, fileList) => handleBeforeUploaderUpdate({file, fileList})}>
      <AntdButton>
        <LocalAntdButtonInner>
          <CloudUploadIcon /> <p>Click to Upload</p>
        </LocalAntdButtonInner>
      </AntdButton>
    </ExtendedAntdUploader>
  );
  return (
    <InputsArea inputsRender={inputsRender} handleClick={handleSendRequest} btnText={"Отправить"} />
  );
};

export default InputsGroup;

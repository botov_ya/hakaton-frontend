import styled from "styled-components";
import {Upload} from "antd";

const ExtendedAntdUploader = styled(Upload)`
  width: 100%;
  & .ant-upload-list {
    position: absolute;
    width: 300px;
    max-width: 100%;
  }
  & > div {
    width: 100%;
  }
  & button {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

const LocalAntdButtonInner = styled.div`
  margin: 0 -5px;
  display: flex;
  & > * {
    margin: 0 5px;
  }
`

export const Styled = {ExtendedAntdUploader, LocalAntdButtonInner}
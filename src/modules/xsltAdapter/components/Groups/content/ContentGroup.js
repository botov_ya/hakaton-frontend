import React from "react";
import XSLTAdapterTable from "../../Table/XSLTAdapterTable";
import { Box } from "@material-ui/core";
import { parseTableHeader } from "../../Table/utils/parseTableHeaders";
import { parseTableBody } from "../../Table/utils/parseTableBody";

const ContentGroup = ({ parsedData, purchaseEvent }) => {
  const headersRender = parseTableHeader(Object.keys(parsedData.data[0]));
  const bodyRender = parseTableBody(parsedData.data, purchaseEvent);
  return (
    <Box mt={10}>
      <XSLTAdapterTable
        tableHeadRender={headersRender}
        tableBodyRender={bodyRender}
      />
    </Box>
  );
};

export default ContentGroup;

import styled from "styled-components";
import Box from "@material-ui/core/Box";

const LocalInputGroup = styled.div`
  
`

const ExtendedMUIBox = styled(Box)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  flex-wrap: wrap;
  & ${LocalInputGroup} {
    margin: -10px -10px;
    & > * {
          margin: 10px 10px;
    }
  }
`

export const Styled = {ExtendedMUIBox, LocalInputGroup}
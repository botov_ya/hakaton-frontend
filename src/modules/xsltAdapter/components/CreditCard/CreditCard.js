import React from "react";
import Card from "react-credit-cards";
import { Styled } from "./CreditCard.styled";

import {
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
  formatFormData
} from "./utils/utils";

import "react-credit-cards/es/styles-compiled.css";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export default class CreditCard extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    number: "",
    name: "",
    expiry: "",
    cvc: "",
    issuer: "",
    focused: "",
    formData: null
  };

  handleCallback = ({ issuer }, isValid) => {
    if (isValid) {
      this.setState({ issuer });
    }
  };

  handleInputFocus = ({ target }) => {
    this.setState({
      focused: target.name
    });
  };

  handleInputChange = ({ target }) => {
    if (target.name === "number") {
      target.value = formatCreditCardNumber(target.value);
    } else if (target.name === "expiry") {
      target.value = formatExpirationDate(target.value);
    } else if (target.name === "cvc") {
      console.log(target.value.length);
      target.value = formatCVC(target.value);
    }

    this.setState({ [target.name]: target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { issuer } = this.state;
    const formData = [...e.target.elements]
      .filter(d => d.name)
      .reduce((acc, d) => {
        acc[d.name] = d.value;
        return acc;
      }, {});
    this.setState({ formData });
    this.form.reset();
    this.props.handleSubmitPurchase();
  };

  render() {
    const { name, number, expiry, cvc, focused, issuer, formData } = this.state;
    return (
      <div key="Payment">
        <div className="App-payment">
          <Card
            number={number}
            name={name}
            expiry={expiry}
            cvc={cvc}
            focused={focused}
            callback={this.handleCallback}
          />
          <form ref={c => (this.form = c)} onSubmit={this.handleSubmit}>
            <Styled.ExtendedMUIBox p={2} mt={2}>
              <Styled.LocalInputGroup>
                <TextField
                  variant="outlined"
                  type="tel"
                  name="number"
                  className="form-control"
                  placeholder="Card Number"
                  pattern="[\d| ]{16,22}"
                  required
                  onChange={this.handleInputChange}
                  onFocus={this.handleInputFocus}
                />
                <TextField
                  variant="outlined"
                  type="text"
                  name="name"
                  className="form-control"
                  placeholder="Name"
                  required
                  onChange={this.handleInputChange}
                  onFocus={this.handleInputFocus}
                />
              </Styled.LocalInputGroup>
              <Styled.LocalInputGroup>
                <TextField
                  variant="outlined"
                  type="tel"
                  name="expiry"
                  className="form-control"
                  placeholder="Valid Thru"
                  pattern="\d\d/\d\d"
                  required
                  onChange={this.handleInputChange}
                  onFocus={this.handleInputFocus}
                />
                <TextField
                  variant="outlined"
                  type="tel"
                  name="cvc"
                  className="form-control"
                  placeholder="CVC"
                  required
                  onChange={this.handleInputChange}
                  onFocus={this.handleInputFocus}
                />
              </Styled.LocalInputGroup>
              <Styled.LocalInputGroup>
                <TextField
                  variant="outlined"
                  type="hidden"
                  name="issuer"
                  value={issuer}
                />
                <div className="form-actions">
                  <Button
                    type={"submit"}
                    variant={"outlined"}
                    color={"primary"}
                    style={{ fontSize: "1rem" }}
                  >
                    Произвести оплату
                  </Button>
                </div>
              </Styled.LocalInputGroup>
            </Styled.ExtendedMUIBox>
          </form>
        </div>
      </div>
    );
  }
}

import React, { useContext, useState } from "react";
import InputsGroup from "./Groups/inputs/InputsGroup";
import { modulesRequests } from "../../../api/modulesRequests";
import { GlobalContext } from "../../../root/App";
import { alertTypesEnum } from "../../../library/enums/alertTypesEnum";
import ContentGroup from "./Groups/content/ContentGroup";
import PurchaseDialog from "./PurchaseDialog/PurchaseDialog";

const XSLTAdapter = () => {
  const { globalStateHandlers } = useContext(GlobalContext);
  const [fileListState, setFileListState] = useState({ files: [] });
  const [parsedData, setParsedData] = useState({ data: [] });
  const [dialogState, setDialogState] = useState({isOpened: false})

  const {
    handleSetGlobalFetchingStatus,
    handleSetGlobalAlert
  } = globalStateHandlers;

  const handleChangeDialogOpenStatus = (bool) => {
    setDialogState(prevState => ({...prevState, isOpened: bool}))
  }

  const handleSubmitPurchase = () => {
    handleSetGlobalAlert({
      status: "active",
      type: alertTypesEnum.success,
      title: "Успешно",
      message: "Операция была осуществлена"
    });
    handleChangeDialogOpenStatus(false)
  }

  const handleBeforeUploaderUpdate = ({ fileList }) => {
    setFileListState(prevState => ({
      ...prevState,
      files: [...prevState.files, ...fileList]
    }));
    return false;
  };

  const handleUploaderRemoveItem = file => {
    const changedFiles = fileListState.files.filter(
      item => item.uid !== file.uid
    );
    setFileListState(prevState => ({ ...prevState, files: changedFiles }));
  };

  const handleSendRequest = () => {
    handleSetGlobalFetchingStatus(true);
    if (!fileListState.files.length) {
      handleSetGlobalAlert({
        status: "active",
        type: alertTypesEnum.error,
        title: "Произошла ошибка",
        message: "Отсутствуют файлы"
      });
      console.error(`Отсутствуют файлы`);
      return null;
    }
    const formData = new FormData();
    const url = "http://78.24.216.177:8000/xslt_converter/converter/";
    fileListState.files.forEach(file => {
      if (file?.name.includes("xslt")) {
        formData.append("file_xslt", file);
      } else if (file?.name.includes("xml")) {
        formData.append("file_xml", file);
      }
    });
    modulesRequests
      .sendFile(url, formData)
      .then(response => {
        console.log("xslt response", response);
        setParsedData({ data: response });
        handleSetGlobalFetchingStatus(false);
      })
      .catch(e => {
        handleSetGlobalAlert({
          status: "active",
          type: alertTypesEnum.error,
          title: "Произошла ошибка",
          message:
            "Во время обработки запроса что-то пошло не так, проверьте отправляемые файлы"
        });
        console.error(e);
      });
  };

  return (
    <>
      <InputsGroup
        handleSendRequest={handleSendRequest}
        handleBeforeUploaderUpdate={handleBeforeUploaderUpdate}
        handleUploaderRemoveItem={handleUploaderRemoveItem}
      />
      {parsedData.data.length ? <ContentGroup purchaseEvent={() => handleChangeDialogOpenStatus(true)} parsedData={parsedData}/> : null}
      <PurchaseDialog handleSubmitPurchase={handleSubmitPurchase} isOpen={dialogState.isOpened} handleCloseDialog={() => handleChangeDialogOpenStatus(false)}/>
    </>
  );
};

export default XSLTAdapter;

import React from "react";
import TableCell from "@material-ui/core/TableCell";

export function makeTableCell(value) {
    return <TableCell align="left" style={{padding: '1rem'}}>{value}</TableCell>
}
import {makeTableCell} from "./makeTableCell";
import React from "react";
import TableRow from "@material-ui/core/TableRow";

export function parseTableHeader(headersCollection) {
    const headers = headersCollection.map(item => makeTableCell(item))
    headers.push(makeTableCell('Действие'))
    return <TableRow>{headers}</TableRow>
}
import {makeTableCell} from "./makeTableCell";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
import Button from "@material-ui/core/Button";

export function parseTableBody(bodyCollection, purchaseEvent) {
    return bodyCollection.map(item => {
        const values = Object.values(item)
        const row = values.map(item => makeTableCell(item))
        row.push(makeTableCell(<Button onClick={purchaseEvent} variant={"outlined"}>Оплатить</Button>))
        return <TableRow>{row}</TableRow>
    })
}
import React from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import { TableBody } from "@material-ui/core";

const XSLTAdapterTable = ({ tableHeadRender, tableBodyRender }) => {
  return (
    <TableContainer component={Paper}>
      <Table size="small">
        <TableHead>{tableHeadRender}</TableHead>
        <TableBody>{tableBodyRender}</TableBody>
      </Table>
    </TableContainer>
  );
};

export default XSLTAdapterTable;

import React from "react";
import XSLTAdapter from "./components/XSLTAdapter";
import {useDefaultModuleEntryPoint} from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
    const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
        entryModuleComponent: <XSLTAdapter />,
        withAppBar: true
    });
    return <>{defaultReduxModuleEntryPoint}</>;
};

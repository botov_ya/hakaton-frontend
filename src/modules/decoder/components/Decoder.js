import React, { useContext, useState } from "react";
import InputsGroup from "./InputsGroup";
import { GlobalContext } from "../../../root/App";
import { modulesRequests } from "../../../api/modulesRequests";
import { alertTypesEnum } from "../../../library/enums/alertTypesEnum";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";

const initialDecodedDataState = { data: null }

const Decoder = () => {
  const [message, setMessage] = useState("");
  const [decodedData, setDecodedData] = useState(initialDecodedDataState);
  const { globalStateHandlers, globalState } = useContext(GlobalContext);
  const {
    handleSetGlobalFetchingStatus,
    handleSetGlobalAlert
  } = globalStateHandlers;

  const handleChangeMessageState = value => {
    setMessage(value);
  };

  const handleSendRequest = () => {
    handleSetGlobalFetchingStatus(true);
    const errorMessage = {
      status: "active",
      type: alertTypesEnum.error,
      title: "Произошла ошибка",
      message:
        "Произошла ошибка во время декодирования сообщнения, попробуйте еще раз"
    };
    modulesRequests
      .decodeValue(message)
      .then(data => {
        if (data) {
          setDecodedData(prevState => ({ ...prevState, data }));
        } else {
          handleSetGlobalAlert(errorMessage);
          setDecodedData(initialDecodedDataState)
        }
      })
      .catch(e => {
        console.error(e);
        handleSetGlobalAlert(errorMessage);
        setDecodedData(initialDecodedDataState)
      }).finally(() => {
      setMessage('')
    });
  };
  return (
    <>
      <InputsGroup
        value={message}
        onChange={value => handleChangeMessageState(value)}
        handleSendRequest={handleSendRequest}
      />
      {decodedData.data ? (
        <Box mt={5}>
          <Paper>
            <Box p={5}>{JSON.stringify(decodedData.data)}</Box>
          </Paper>
        </Box>
      ) : null}
    </>
  );
};

export default Decoder;

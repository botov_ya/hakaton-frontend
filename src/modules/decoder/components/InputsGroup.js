import React from 'react';
import InputsArea from "../../../library/UI-elements/InputsArea/InputsArea";
import styled from "styled-components";
import TextField from "@material-ui/core/TextField";

const StyledTextField = styled(TextField)`
  width: 100%;
`;

const InputsGroup = ({value, onChange, handleSendRequest}) => {
    const inputsRender = (
        <>
            <StyledTextField
                variant="outlined"
                placeholder={"Введите текст конвертации"}
                value={value}
                onChange={(e) => onChange(e.target.value)}
            />
        </>
    );
    return (
        <InputsArea inputsRender={inputsRender} handleClick={handleSendRequest} btnText={'конвертировать'} />
    );
};

export default InputsGroup;

import React from "react";
import Decoder from "./components/Decoder";
import { useDefaultModuleEntryPoint } from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <Decoder />,
    withAppBar: true
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

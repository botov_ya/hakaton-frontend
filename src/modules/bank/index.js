import React from "react";
import BankMainPage from "./components/BankMainPage";
import { useDefaultModuleEntryPoint } from "../../library/hooks/useDefaultModuleEntryPoint";

export default () => {
  const defaultReduxModuleEntryPoint = useDefaultModuleEntryPoint({
    entryModuleComponent: <BankMainPage />,
    withAppBar: true,
  });
  return <>{defaultReduxModuleEntryPoint}</>;
};

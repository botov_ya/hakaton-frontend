import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { protectedRequests } from "../../../../api/protectedRequests";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

const maskPan = (pan) => {
  const lengthOfPan = pan.length;
  const lastDigits = pan.slice(Math.max(lengthOfPan - 4, 0));
  let masked = "*".repeat(lengthOfPan - 4);
  return masked + " " + lastDigits;
};

export const CardsPage = (props) => {
  const [cards, setCards] = useState([]);
  useEffect(() => {
    protectedRequests.getCards().then((res) => {
      setCards(res);
    });
  }, []);
  const classes = useStyles();
  return (
    <React.Fragment>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Банк</TableCell>
            <TableCell>Владелец карты</TableCell>
            <TableCell>Pan</TableCell>
            <TableCell>Годна до</TableCell>
            <TableCell>Балланс</TableCell>
            <TableCell align="right">История операций</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {cards.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.owner}</TableCell>
              <TableCell>{maskPan(row.pan)}</TableCell>
              <TableCell>{row.expired_date}</TableCell>
              <TableCell>{row.balance}</TableCell>
              <TableCell align="right">
                <Link to={`/bank/cards/${row.id}/transactions`}>
                  <Button onChange={() => {}}>Показать</Button>
                </Link>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}></div>
    </React.Fragment>
  );
};

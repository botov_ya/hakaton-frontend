import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { protectedRequests } from "../../../../api/protectedRequests";
import { Link, useParams } from "react-router-dom";
import moment from "moment";
import Button from "@material-ui/core/Button";

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

const maskPan = (pan) => {
  const lengthOfPan = pan.length;
  const lastDigits = pan.slice(Math.max(lengthOfPan - 4, 0));
  let masked = "*".repeat(lengthOfPan - 4);
  return masked + " " + lastDigits;
};

export const TransactionPage = (props) => {
  const [transactions, setTransactions] = useState([]);
  const params = useParams("/bank/cards/:cardId/transactions");
  useEffect(() => {
    const { idCard } = params;
    protectedRequests.getTransactions(idCard).then((res) => {
      setTransactions(res);
    });
  }, []);

  const classes = useStyles();
  return (
    <React.Fragment>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Банк эммиттер</TableCell>
            <TableCell>Контрагент</TableCell>
            <TableCell>Дата</TableCell>
            <TableCell>Тип операции</TableCell>
            <TableCell align="right">Сумма операции</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {transactions.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.card_name}</TableCell>
              <TableCell>{row.contr_agent}</TableCell>
              <TableCell>
                {moment(row.date).format("DD.MM.YYYY HH:mm:ss")}
              </TableCell>
              <TableCell>{row.operation_type}</TableCell>
              <TableCell align="right">{row.sum}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link to={`/bank/cards/`}>
          <Button onChange={() => {}}>К списку карт</Button>
        </Link>
      </div>
    </React.Fragment>
  );
};

import React, { useContext, useState } from "react";

import { Switch, Route, Redirect } from "react-router-dom";
import { LoginPage } from "./pages/LoginPage";
import { CardsPage } from "./pages/CardsPage";
import { TransactionPage } from "./pages/TransactionPage";
import Box from "@material-ui/core/Box";

export const BankContext = React.createContext(null);

const BankMainPage = (props) => {
  const [bankContext, setBankContext] = useState({});
  return (
    <BankContext.Provider value={{ bankContext, setBankContext }}>
      <Box mt={5}>
        <Switch>
          <Route path={"/bank/login"}>
            <LoginPage />
          </Route>
          {bankContext?.isAuth && (
            <Route path={"/bank/cards/:idCard/transactions"}>
              <TransactionPage />
            </Route>
          )}
          {bankContext?.isAuth && (
            <Route exact path={"/bank/cards"}>
              <CardsPage />
            </Route>
          )}
          <Redirect to={"/bank/login"}>
            <LoginPage />
          </Redirect>
        </Switch>
      </Box>
    </BankContext.Provider>
  );
};

export default BankMainPage;

import * as axios from "axios";
import { axiosInstance } from "./axiosGuest";

const doCleanup = () => {
  localStorage.removeItem("token_access");
  localStorage.removeItem("token_refresh");
  localStorage.removeItem("user");
};

const axiosTemplate = axios.create({
  baseURL: "http://78.24.216.177:8000/",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
});

axiosTemplate.interceptors.request.use(
  (config) => {
    if (localStorage.getItem("access_token") !== "undefined") {
      config.headers.Authorization = `Bearer ${localStorage.getItem(
        "access_token"
      )}`;
      return config;
    } else {
      doCleanup();
      return Promise.reject("unauthorised");
    }
  },
  (error) => {
    return Promise.reject(error);
  }
);

const successHandler = (response) => {
  return response;
};

const errorHandler = (error) => {
  if (error?.response?.status == 401) {
    axiosInstance
      .post(
        "/api/token/refresh/",
        {
          refresh: localStorage.getItem("refresh_token"),
          username: localStorage.getItem("user"),
        },
        {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        }
      )
      .then((response) => {
        localStorage.setItem("token_refresh", response.data.access);
        error.config.Authorization = `Bearer ${localStorage.getItem(
          "token_access"
        )}`;
        securedAxiosInstance(error);
      })
      .catch((error) => {
        doCleanup();
        sessionStorage.setItem("redirect_to", window.location);
        window.location = "/bank/login";

        return Promise.reject(error);
      });
  } else {
  }
};

axiosTemplate.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error)
);

const securedAxiosInstance = axiosTemplate;

export default securedAxiosInstance;

import * as axios from 'axios'

export const axiosInstance = axios.create({
    baseURL : 'http://78.24.216.177:8000/',
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
    }
});

import * as d3ColorScale from "d3-scale-chromatic";
import * as d3Color from "d3-color";

export const getColorInRangeLinear = (current, max) => {
	if (!current && !max) return [255,255,255];
	const position = current/max;
	const palette = d3ColorScale.interpolateTurbo(position);
	const color = d3Color.color(palette).formatHex()
	return color;
}

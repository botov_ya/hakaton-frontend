import * as d3ColorScale from "d3-scale-chromatic";
import * as d3Color from "d3-color";

export const getColorInRangeRedGreen = (current, max) => {
	if (!current && !max) return [255,255,255];
	const position = 1 - current/max;
	const palette = d3ColorScale.interpolateReds(position);
	const color = d3Color.color(palette).formatHex()
	return color;
}

export const alertTypesEnum = {
    error: 'error',
    success: 'success',
    warning: 'warning',
    default: 'info'
};
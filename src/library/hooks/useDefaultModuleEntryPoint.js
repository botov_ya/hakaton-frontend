import React from "react";
import Container from "@material-ui/core/Container";
import CustomAppBar from "../UI-elements/CustomAppBar/CustomAppBar";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Toolbar from "@material-ui/core/Toolbar";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

const StyledMUIButton = styled(Button)`
  font-size: 0.8rem;
  color: #fff;
  text-decoration: none;
  outline: none;
  & .MuiSvgIcon-root {
    font-size: 15px;
  }
`;

export function useDefaultModuleEntryPoint({
  entryModuleComponent,
  withAppBar=true
}) {
  return (
    <>
      <Container style={{ maxWidth: "100%", height: "100%" }} disableGutters>
        {withAppBar && (
          <CustomAppBar>
            <Link to={"/"}>
              <StyledMUIButton
                variant="outlined"
                startIcon={<ArrowBackIosIcon />}
                color={"inherit"}
              >
                На главную
              </StyledMUIButton>
            </Link>
          </CustomAppBar>
        )}
        <Container style={{ height: "100%" }} maxWidth="lg">
          {entryModuleComponent}
        </Container>
      </Container>
    </>
  );
}

const CutStringWithEllipsisOnTheEnd = () => `
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const HideScrollbar = () => `
  &::-webkit-scrollbar {
    display: none;
  }
`;

const firstLetterUppercase = () => `
  &:first-letter {
    text-transform: uppercase;
  }
`;

const AddMarginsProps = (props) =>
  `margin: ${props.m ?? ""};
    margin-top: ${props.mt ?? ""};
    margin-bottom: ${props.mb ?? ""};
    margin-left: ${props.ml ?? ""};
    margin-right: ${props.mr ?? ""};`;

const AddPaddingsProps = (props) => `
    padding: ${props.p ?? ""};
    padding-top: ${props.pt ?? ""};
    padding-bottom: ${props.pb ?? ""};
    padding-left: ${props.pl ?? ""};
    padding-right: ${props.pr ?? ""};
`;

const AddSizeProps = (props) => `
  width: ${props?.size?.width ?? 'auto'};
  max-width: ${props?.size?.maxWidth ?? '100%'};
  min-width: ${props?.size?.minWidth ?? 'auto'};
  height: ${props?.size?.height ?? 'auto'};
  min-height: ${props?.size?.minHeight ?? 'auto'};
  max-height: ${props?.size?.maxHeight ?? 'auto'};
`

const nonNullishCondition = (condition, fallback) =>
  condition ?? fallback;

export const Styled = {
  offset: {
    AddMarginsProps,
    AddPaddingsProps,
  },
  text: {
    CutStringWithEllipsisOnTheEnd,
    firstLetterUppercase,
  },
  color: {},
  size: {
    AddSizeProps
  },
  other: {
    HideScrollbar,
    nonNullishCondition,
  },
};

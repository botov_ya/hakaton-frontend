import React from "react";
import Button from "@material-ui/core/Button";


const TestButton = (props) => {
	return (
		<Button variant="contained" color="secondary">
			Secondary
		</Button>
	)
}

export default TestButton

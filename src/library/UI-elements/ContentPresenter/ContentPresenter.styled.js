import styled from "styled-components";

const LocalMainWrapper = styled.div`
  height: fit-content;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin: -10px -10px;
  & > div {
    height: 100%;
    margin: 10px 10px;
    width: calc(100% / 12 * 3 - 20px);
    min-width: 200px;
    & > .MuiPaper-root {
      width: 100%;
      height: 100%;
      min-height: 240px;
      display: flex;
      justify-content: center;
      flex-direction: column;
      align-items: center;
    }
  }
`

export const Styled = {LocalMainWrapper}
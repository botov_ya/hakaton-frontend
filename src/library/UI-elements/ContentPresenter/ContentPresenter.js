import React from "react";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { Styled } from "./ContentPresenter.styled";

const ContentPresenter = ({ children }) => {
  return (
    <Styled.LocalMainWrapper>
      {children.map(child => (
        <Box mt={2}>
          <Paper>
            <Box p={2}>{child}</Box>
          </Paper>
        </Box>
      ))}
    </Styled.LocalMainWrapper>
  );
};

export default ContentPresenter;

import React, { createContext } from "react";
import PropTypes from "prop-types";
import { Portal } from "react-portal";
import { Styled } from "./ScreenCornerPortal.styled";

export const PortalContext = createContext(null);

const ScreenCornerPortal = ({ closePortal = () => {}, isOpen, children }) => {
  return (
    <PortalContext.Provider value={{ closePortal, isOpen }}>
      {isOpen ? (
        <Portal>
          <Styled.LocalContentWrap>{children}</Styled.LocalContentWrap>
        </Portal>
      ) : null}
    </PortalContext.Provider>
  );
};

export default ScreenCornerPortal;

ScreenCornerPortal.propTypes = {
  children: PropTypes.node,
  closePortal: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  openPortal: PropTypes.func.isRequired,
};

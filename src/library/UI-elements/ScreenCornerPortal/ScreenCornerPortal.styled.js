import styled from "styled-components";

const LocalContentWrap = styled.section`
  position: fixed;
  right: 25px;
  bottom: 25px;
  z-index: 9000;
`

export const Styled = {LocalContentWrap}
import React from 'react';
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import styled from "styled-components";

const StyledGridItem = styled(Grid)`
  display: flex;
  align-items: center;
  & > button {
    height: 100%;
    width: 100%;
  }
`;

const InputsArea = ({inputsRender, handleClick, btnText="Узнать"}) => {
    return (
        <Box mt={5}>
            <Grid container>
                <StyledGridItem item md={10} sm={9} xs={7}>
                    {inputsRender}
                </StyledGridItem>
                <StyledGridItem item xs={2}>
                    <Button
                        style={{minWidth: '120px'}}
                        variant="contained"
                        color="primary"
                        onClick={handleClick}
                    >
                        {btnText}
                    </Button>
                </StyledGridItem>
            </Grid>
        </Box>
    );
};

export default InputsArea;

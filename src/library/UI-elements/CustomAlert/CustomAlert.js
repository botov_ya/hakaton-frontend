import React from "react";
import PropTypes from "prop-types";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";
import { alertTypesEnum } from "../../enums/alertTypesEnum";
import { Styled } from "./CustomAlert.styled";
import ScreenCornerPortal from "../ScreenCornerPortal/ScreenCornerPortal";
import { usePortalContext } from "./utils/usePortalContext";

const CustomAlert = ({
  type,
  title,
  message,
  size,
  style: userStyles = {},
  onClose,
  ...otherProps
}) => {
  const { enhancedCloseCallback } = usePortalContext(onClose);
  return (
    <Styled.LocalMainWrapper size={size} style={userStyles} {...otherProps}>
      <Alert
        severity={type}
        onClose={() => enhancedCloseCallback()}
        {...otherProps}
      >
        <AlertTitle>{title}</AlertTitle>
        <div>{message}</div>
      </Alert>
    </Styled.LocalMainWrapper>
  );
};

const Usage = (props) => {
  return (
    <ScreenCornerPortal {...props}>
      <CustomAlert {...props} />
    </ScreenCornerPortal>
  );
};

export default Usage;

CustomAlert.propTypes = {
  message: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
  size: PropTypes.object,
  title: PropTypes.node.isRequired,
  type: PropTypes.oneOf([
    alertTypesEnum.success,
    alertTypesEnum.warning,
    alertTypesEnum.error,
    alertTypesEnum.default,
  ]).isRequired,
  userStyles: PropTypes.object,
};

import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";

const CustomAppBar = ({children, className}) => {
  return (
    <AppBar className={`${className}`} position={"static"}>
      <Toolbar>
          {children}
      </Toolbar>
    </AppBar>
  );
};

export default CustomAppBar;

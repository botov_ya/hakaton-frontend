import React from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { getColorInRangeLinear } from "../../services/getColorInRangeLinear";

const mockData = [
  {
    name: "Page A",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Page B",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Page C",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Page D",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "Page E",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Page F",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: "Page G",
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];
const mockXName = "name";
const mockYName = null;
const mockBarFieldNames = ["pv", "uv", "amt"];

const prepareXAxe = (fieldName) => {
  if (fieldName) return <XAxis dataKey="name" />;
  return <XAxis />;
};

const prepareYAxe = (fieldName) => {
  if (fieldName) return <YAxis dataKey="name" />;
  return <YAxis />;
};

const prepareBars = (linesNamesArray) => {
  return linesNamesArray.map((lineName, index) => {
    const color = getColorInRangeLinear(index, linesNamesArray.length - 1);
    return <Bar dataKey={lineName} stackId="a" fill={color} />;
  });
};

export const BarChartBuilder = ({
  data = [],
  xFieldName = null,
  yFieldName = null,
  barFieldNames = [],
  showTooltip = true,
  showLegend = true,
  useMockData = false,
}) => {
  const preparedData = useMockData ? mockData : data;
  const PreparedXAxe = prepareXAxe(useMockData ? mockXName : xFieldName);
  const PreparedYAxe = prepareYAxe(useMockData ? mockYName : yFieldName);
  const PreparedBars = prepareBars(
    useMockData ? mockBarFieldNames : barFieldNames
  );

  return (
    <BarChart
      width={500}
      height={300}
      data={preparedData}
      margin={{
        top: 20,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      {PreparedXAxe}
      {PreparedYAxe}
      {showTooltip && <Tooltip />}
      {showLegend && <Legend />}
      {PreparedBars}
    </BarChart>
  );
};

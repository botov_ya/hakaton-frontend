import React from "react";
import { PieChart, Pie, Tooltip, Legend, Cell } from "recharts";
import { getColorInRangeLinear } from "../../services/getColorInRangeLinear";

const mockData = [
  {
    name: "Bulling",
    value: 90,
    fill: "red"
  },
  {
    name: "Not Bulling",
    value: 10,
    fill: "red"
  }
];

const preparePie = data => {
  return (
    <Pie data={data} cx="50%" cy="50%" outerRadius={60} dataKey="value" label>
      {data.map((entry, index) => {
        return <Cell key={`cell-${index}`} fill={entry.fill} />;
      })}
    </Pie>
  );
};

export const PieChartBuilder = ({
  data = [],
  showTooltip = true,
  showLegend = true,
  useMockData = false
}) => {
  const preparedData = useMockData ? mockData : data;
  console.log(preparedData);
  const PreparedBars = preparePie(preparedData);

  return (
    <PieChart width={200} height={200}>
      {showTooltip && <Tooltip />}
      {showLegend && <Legend />}
      {PreparedBars}
    </PieChart>
  );
};

import React, { useCallback, useEffect, useState } from "react";
import styled from "styled-components";

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const StyledRadar = styled.div`
  position: relative;
  width: 400px;
  height: 400px;
  margin: 20px auto;
  border: 3px solid #0c0;
  background-color: #020;
  border-radius: 50%;

  & > * {
    position: absolute;
  }
  .beacon {
    left: 50%;
    top: 50%;
    border-style: solid;
    border-width: 8px 200px 8px 0;
    border-color: transparent;
    margin-top: -8px;
    transform-origin: 0 50%;
  }
  #beacon {
    border-right-color: #0c0;
    animation: spin 2s 0s linear infinite;
  }
  #beacon-75 {
    border-right-color: rgba(0, 204, 0, 0.75);
    animation: spin 2s 0.03s linear infinite;
  }
  #beacon-50 {
    border-right-color: rgba(0, 204, 0, 0.5);
    animation: spin 2s 0.06s linear infinite;
  }
  #beacon-25 {
    border-right-color: rgba(0, 204, 0, 0.25);
    animation: spin 2s 0.09s linear infinite;
  }
  .circle {
    left: 50%;
    top: 50%;
    border: 1px solid #0c0;
    border-radius: 50%;
  }

  #circle-big {
    width: 300px;
    height: 300px;
    margin: -150px;
  }
  #circle-medium {
    width: 200px;
    height: 200px;
    margin: -100px;
  }
  #circle-small {
    width: 100px;
    height: 100px;
    margin: -50px;
  }
  #dot {
    width: 8px;
    height: 8px;
    margin: -4px;
    background-color: #0c0;
  }
  #vertical {
    left: 50%;
    top: 0;
    bottom: 0;
    border-left: 1px solid #0c0;
  }
  #horizontal {
    top: 50%;
    left: 0;
    right: 0;
    border-top: 1px solid #0c0;
  }

  @keyframes spin {
    from {
      transform: rotate(0);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;

const StyledDot = styled.div`
  position: absolute;
  left: ${(props) => props.left}%;
  top: ${(props) => props.right}%;
  border: 1px solid #0c0;
  border-radius: 50%;
  animation: hide 2s 0.09s linear infinite;

  @keyframes hide {
    from {
      transition-property: opacity;
      transition-duration: 4s;
      transition-delay: ${(props) => props.delay}s;
      opacity: 0;
    }
    to {
      transition-property: opacity;
      opacity: 1;
    }
  }
`;

export const RadarAnimation = () => {
  const generateRandomDots = () => {
    const result = [];
    const number = getRandomInt(50) + 20;
    for (let it = 0; it < number; it++) {
      const left = getRandomInt(70) + 20;
      const right = getRandomInt(70) + 20;
      const delay = getRandomInt(4);
      result.push(<StyledDot left={left} right={right} delay={delay} />);
    }
    return result;
  };

  const [Dots, setDots] = useState(generateRandomDots());

  useEffect(() => {
    setTimeout(() => {
      setDots(generateRandomDots());
    }, 4000);
  }, [Dots]);

  return (
    <StyledRadar id="radar">
      <div className="beacon" id="beacon" />
      <div className="beacon" id="beacon-75" />
      <div className="beacon" id="beacon-50" />
      <div className="beacon" id="beacon-25" />
      <div className="circle" id="circle-big" />
      <div className="circle" id="circle-medium" />
      <div className="circle" id="circle-small" />
      <div className="circle" id="dot" />
      <div id="vertical" />
      <div id="horizontal" />
      {Dots}
    </StyledRadar>
  );
};

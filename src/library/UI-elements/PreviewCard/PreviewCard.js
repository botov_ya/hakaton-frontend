import React from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardContent from "@material-ui/core/CardContent";
import { Styled } from "./PreviewCard.styled";

const PreviewCard = ({ style: userStyles = {}, title=null, size, children, iconRender=null, className }) => {
  return (
    <Styled.LocalMainWrapper className={`${className}`} size={size} style={userStyles}>
      <Card>
        {iconRender && <CardContent>
          <Styled.LocalIconWrap>
            {iconRender}
          </Styled.LocalIconWrap>
        </CardContent>}
        <CardContent>
          {title && <Typography component="h6" variant="h6">
            {title}
          </Typography>}
          {children}
        </CardContent>
      </Card>
    </Styled.LocalMainWrapper>
  );
};
export default PreviewCard;

import styled from "styled-components";
import { Styled as styledMixins } from "../../styleMixins/styleMixins";

const { AddSizeProps } = styledMixins.size;
const { AddMarginsProps, AddPaddingsProps } = styledMixins.offset;

const LocalMainWrapper = styled.figure`
  margin: 0;
  padding: 0;
  ${props => AddSizeProps(props)};
  ${props => AddMarginsProps(props)};
  ${props => AddPaddingsProps(props)};
  text-align: center;
  //background:linear-gradient(135deg, #13f1fc 0%,#0470dc 100%) !important;
  & .MuiCardContent-root:not(:first-of-type) {
    padding-top: 0;
  }
`;

const LocalIconWrap = styled.figcaption`
  display: flex;
  justify-content: center;
`;

export const Styled = { LocalMainWrapper, LocalIconWrap };

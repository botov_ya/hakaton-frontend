import React from "react";
import "./App.css";
import "antd/dist/antd.css";
import "@csstools/normalize.css";
import "react-credit-cards/es/styles-compiled.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import DecoderEntry from "../modules/decoder";
import BullingCheckerEntry from "../modules/bullingChecker";
import EmailCheckerEntry from "../modules/emailChecker";
import XSLTAdapterEntry from "../modules/xsltAdapter";
import ScannerUrlEntry from "../modules/scanner";
import BankEntry from "../modules/bank";
import { useGlobalState } from "./utils/hooks/useGlobalState";
import HomeEntry from "../modules/home";
import CustomAlert from "../library/UI-elements/CustomAlert/CustomAlert";

export const GlobalContext = React.createContext({});

function App() {
  const { globalState, globalStateHandlers } = useGlobalState();
  const { alertData, isFetching } = globalState;
  return (
    <GlobalContext.Provider value={{ globalState, globalStateHandlers }}>
      <BrowserRouter>
        <Switch>
          <Route path="/decoder">
            <DecoderEntry />
          </Route>

          <Route path="/bank">
            <BankEntry />
          </Route>

          <Route path="/bulling_checker">
            <BullingCheckerEntry />
          </Route>

          <Route path="/email_checker">
            <EmailCheckerEntry />
          </Route>

          <Route path="/site_scanner">
            <ScannerUrlEntry />
          </Route>

          <Route path="/xslt_adapter">
            <XSLTAdapterEntry />
          </Route>

          <Route path="/">
            <HomeEntry />
          </Route>
        </Switch>
      </BrowserRouter>
      {alertData.status === "active" && (
        <CustomAlert
          type={alertData.type}
          title={alertData.title}
          message={alertData.message}
          size={{ width: "450px" }}
          onClose={() => {
            globalStateHandlers.handleResetGlobalAlert();
          }}
          isOpen={true}
        />
      )}
    </GlobalContext.Provider>
  );
}

export default App;

import { useState } from "react";
import { alertTypesEnum } from "../../../library/enums/alertTypesEnum";

const initialAlertDataState = {
  alertData: {
    status: "inactive",
    type: alertTypesEnum.default,
    title: null,
    message: null,
  },
};

const initialIsFetchingState = {
  isFetching: false,
};

export function useGlobalState() {
  const [alertData, setAlertData] = useState(initialAlertDataState);
  const [isFetchingState, setIsFetchingState] = useState(
    initialIsFetchingState
  );
  const globalStateHandlers = {
    handleSetGlobalAlert: (newAlert) => {
      setAlertData({alertData: newAlert});
    },
    handleResetGlobalAlert: () => {
      setAlertData(initialAlertDataState);
    },
    handleSetGlobalFetchingStatus: (bool) => {
      setIsFetchingState({ isFetching: bool });
    },
  };

  const globalState = {
    ...alertData,
    ...isFetchingState,
  };

  return { globalState, globalStateHandlers};
}
